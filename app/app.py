from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

import pandas as pd
import plotly.express as px

from .models import StockPrice, create_price_stock_view, create_stock_ticker_view
from .schema import StockPrice as StockPriceSchema

from .database import db

from .external.etl import load_generated_stock_data

app = FastAPI(title="Generated Stocks API")
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

@app.on_event("startup")
async def startup():
    await db.connect()
    await create_price_stock_view()
    await create_stock_ticker_view()

@app.on_event("startup")
@repeat_every(seconds=1, raise_exceptions=True)
async def update_stock_prices() -> None:
    await load_generated_stock_data.main()
    
@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()

async def get_stocks_prices_as_dataframe(ticker: str = None):
    """Create a dataframe with all stock price data"""
    if ticker is None:
        stock_price_records = await StockPrice.get_distinct_stocks_and_prices()
        columns = ["stock_id", "ticker", "description", "price", "last_update"]
    else:
        stock_price_records = await StockPrice.get_stock_and_prices_by_ticker(ticker)
        columns=["id", "stock_id", "ticker", "description", "price", "last_update"]

    stock_price_dict = [dict(record) for record in stock_price_records]

    df = pd.DataFrame.from_records(
        stock_price_dict,
        columns=columns,
    )
    return df

async def get_fig_from_dataframe(df: pd.DataFrame, limit: int, title:str):
    """Create a html fig with dataframe"""
    df = df.tail(limit) if limit else df
    fig = px.line(df, title=title, x="last_update", y="price")
    return fig.to_html(full_html=False, include_plotlyjs="cdn")

async def get_stock_tickers():
    """Get stock tickers in DB"""
    stock_tickers_record = await StockPrice.get_stock_tickers()
    stock_tickers_dict = [dict(record) for record in stock_tickers_record]

    return stock_tickers_dict

async def get_stock_tickers_only(limit: int = None):
    """Get stock tickers in DB"""
    _limit = limit
    stock_tickers_record = await StockPrice.get_stock_tickers()
    stock_tickers_list = []
    for record in stock_tickers_record:
        if _limit:
            stock_tickers_list = [ dict(record)["ticker"] ]
            _limit -= 1
        break

    return stock_tickers_list
