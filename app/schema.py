from datetime import datetime
from pydantic import BaseModel

class StockPrice(BaseModel):
    stock_id: int
    ticker: str
    description: str = None
    price: int
    last_update: datetime

    class Config:
        orm_mode = True