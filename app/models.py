from sqlalchemy import Table, Column, String, Integer, DateTime, text
from sqlalchemy_views import CreateView

from .database import db, metadata

stock_prices = Table(
    "stock_prices",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("stock_id", Integer),
    Column("ticker", String(length=10)),
    Column("description", String),
    Column("price", Integer),
    Column("last_update", DateTime),
)

stock_price_view = Table(
    'stock_price_view',
    metadata,
    Column("stock_id", Integer),
    Column("ticker", String(length=10)),
    Column("description", String),
    Column("price", Integer),
    Column("last_update", DateTime),
)

stock_ticker_view = Table(
    'stock_ticker_view',
    metadata,
    Column("stock_id", Integer),
    Column("ticker", String(length=10)),
    Column("last_update", DateTime),
)

async def create_price_stock_view():
    """Creates a view from stock and price tables"""
    definition = text(
        """
        SELECT DISTINCT ON (s.stock_id) s.stock_id, s.ticker, 
        s.description, s.price, s.last_update
        FROM stock_prices s
        ORDER BY s.stock_id, s.last_update DESC
        """
    )
    create_view = CreateView(stock_price_view, definition, or_replace=True)
    await db.execute(create_view)

async def create_stock_ticker_view():
    """Creates a view for stock ticker"""
    definition = text(
        """
        SELECT DISTINCT ON (stock_id) stock_id, ticker, last_update
        FROM stock_prices
        ORDER BY stock_id, last_update DESC
        """
    )
    create_view = CreateView(stock_ticker_view, definition, or_replace=True)
    await db.execute(create_view)

class StockPrice:
    @classmethod
    async def get_stocks_and_prices(cls):
        query = stock_prices.select()
        stock = await db.fetch_all(query)
        return stock

    @classmethod
    async def get_distinct_stocks_and_prices(cls):
        query = stock_price_view.select().order_by(stock_price_view.c.price.desc())
        stock = await db.fetch_all(query)
        return stock

    @classmethod
    async def get_stock_by_id(cls, id: int):
        query = stock_price_view.select().where(stock_price_view.c.stock_id == id)
        stock = await db.fetch_one(query)
        return stock
    
    @classmethod
    async def get_stock_and_price_by_ticker(cls, ticker: str):
        query = stock_price_view.select().where(stock_price_view.c.ticker == ticker.upper())
        stock = await db.fetch_one(query)
        return stock
    
    @classmethod
    async def get_stock_and_prices_by_ticker(cls, ticker: str):
        query = stock_prices.select().where(stock_prices.c.ticker == ticker.upper()).order_by(stock_prices.c.id)
        stock = await db.fetch_all(query)
        return stock
    
    @classmethod
    async def get_stock_tickers(cls):
        query = stock_ticker_view.select()
        stock_tickers = await db.fetch_all(query)
        return stock_tickers

