"""Loads data from Generated Stock API
@ localhost:8000"""

import os
import aiohttp

from dotenv import load_dotenv
from sqlalchemy import Table

import sys
from pathlib import Path

if __name__ == '__main__' and __package__ is None:
    file = Path(__file__).resolve()
    parent, top = file.parent, file.parents[3]
    print(parent, top)

    sys.path.append(str(top))
    try:
        sys.path.remove(str(parent))
    except ValueError: # Already removed
        pass

    import app.external.etl
    __package__ = 'app.external.etl'

from ...models import stock_prices
from ...app import db
from ...schema import StockPrice as StockPriceSchema


BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
load_dotenv(os.path.join(BASE_DIR, ".env"))

async def get_data(session: aiohttp.ClientSession, url: str):
    """Get all stock data from API"""
    async with session.get(url, verify_ssl=False) as resp:
        resp_json = await resp.json()
        return resp_json if resp.status == 200 else None

async def store_data(table: Table, data: list[dict]):
    """Store given data in table"""
    if not data: return
    for row in data:
        stock_price_row = StockPriceSchema(**row).dict()
        query = table.insert().values(**stock_price_row)
        await db.execute(query)


async def main():
    stocks_api_url=os.environ["GENERATED_STOCK_API_STOCKS_URL"]

    async with aiohttp.ClientSession() as session:
        all_stocks_data_json = await get_data(session, stocks_api_url)
    
    await store_data(stock_prices, all_stocks_data_json)
