"""Main entry for Fastapi App"""
import uvicorn
from fastapi.responses import HTMLResponse
from fastapi import Request


from app.app import app, get_fig_from_dataframe,\
    get_stock_tickers, get_stocks_prices_as_dataframe, templates


@app.get("/stocks/", response_class=HTMLResponse)
async def show_stocks(request: Request):
    """A template with top 10 stocks and
    chart window"""
    df_all_stock_prices = await get_stocks_prices_as_dataframe()
    df_last = df_all_stock_prices.head(10).to_html()
    stock_tickers = await get_stock_tickers()
    return templates.TemplateResponse(
        "stock-chart.html",
        {
            "request": request,
            "top10_table": df_last,
            "stock_tickers": stock_tickers,
        },
    )

@app.get("/stock/chart/{ticker}", response_class=HTMLResponse)
async def show_stock_chart(request: Request, ticker: str):
    """A template with chart of specified ticker"""
    df_all_stock_prices = await get_stocks_prices_as_dataframe(ticker)
    fig_stock_line = await get_fig_from_dataframe(df_all_stock_prices, 0, ticker.upper())
    return templates.TemplateResponse(
        "chart.html",
        {
            "request": request,
            "chart": fig_stock_line,
        },
    )

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
